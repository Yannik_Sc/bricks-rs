use crate::components::{
    AbilityPickup, Ball, BallBundle, BallContainer, BallTextureHandleHolder, EndWall, Pedal,
    StickingBall,
};
use crate::{GameMenuState, Layer, LevelData};
use bevy::prelude::*;
use heron::prelude::*;
use std::ops::DerefMut;

impl Default for Ball {
    fn default() -> Self {
        Self { size: 1.0 }
    }
}

impl BallBundle {
    pub fn new(texture: Handle<Image>) -> Self {
        Self::new_base(texture, RigidBody::Dynamic)
    }

    pub fn new_static(texture: Handle<Image>) -> Self {
        Self::new_base(texture, RigidBody::Static)
    }

    fn new_base(texture: Handle<Image>, body: RigidBody) -> Self {
        Self {
            ball: Ball::default(),
            body,
            layers: CollisionLayers::new(Layer::Bricks, Layer::Bricks),
            collision_shape: CollisionShape::Sphere { radius: 10.0 },
            material: PhysicMaterial {
                friction: 0.0,
                density: 1000.0,
                restitution: 1.0,
            },
            velocity: Velocity::from(Vec2::new(200.0, 200.0)),
            sprite: SpriteBundle {
                transform: Transform::from_xyz(1000.0, 1000.0, 0.0),
                global_transform: GlobalTransform::from_xyz(1000.0, 1000.0, 0.0),
                sprite: Sprite {
                    custom_size: Some(Vec2::new(20.0, 20.0)),
                    ..Default::default()
                },
                texture,
                ..Default::default()
            },
        }
    }
}

impl Default for BallBundle {
    fn default() -> Self {
        Self::new(Default::default())
    }
}

impl From<Vec2> for StickingBall {
    fn from(vec: Vec2) -> Self {
        Self {
            pedal_offset: vec.extend(0.0),
        }
    }
}

pub fn ball_sync_system(mut query: Query<(&Ball, &mut Transform, &mut CollisionShape)>) {
    for (ball, mut transform, mut shape) in query.iter_mut() {
        transform.scale = Vec3::ONE * ball.size;

        if let CollisionShape::Sphere { radius } = shape.deref_mut() {
            *radius = 10.0 * ball.size;
        }
    }
}

pub fn ball_hit_pedal_system(
    pedals: Query<(Entity, &Pedal, &Transform)>,
    mut balls: Query<(Entity, &mut Ball, &mut RigidBody, &Transform)>,
    mut events: EventReader<CollisionEvent>,
    mut commands: Commands,
) {
    for event in events.iter() {
        if event.is_started() {
            continue;
        }

        let (entity1, entity2) = event.rigid_body_entities();

        let (_, pedal, pedal_transform) = if let Ok(pedal) = pedals.get(entity1) {
            pedal
        } else if let Ok(pedal) = pedals.get(entity2) {
            pedal
        } else {
            continue;
        };

        let (ball_entity, _, mut body, ball_transform) = if let Ok(ball) = balls.get_mut(entity1) {
            ball
        } else if let Ok(ball) = balls.get_mut(entity2) {
            ball
        } else {
            continue;
        };

        if !pedal.sticky {
            continue;
        }

        let offset = Vec3::new(
            ball_transform.translation.x - pedal_transform.translation.x,
            ball_transform.translation.y - pedal_transform.translation.y,
            0.0,
        );

        commands.entity(ball_entity).insert(StickingBall {
            pedal_offset: offset,
        });
        *body.deref_mut() = RigidBody::Static;
    }
}

pub fn ball_hit_end_wall_system(
    mut commands: Commands,
    balls: Query<Entity, With<Ball>>,
    mut events: EventReader<CollisionEvent>,
    walls: Query<Entity, With<EndWall>>,
) {
    for event in events.iter() {
        let (entity1, entity2) = event.rigid_body_entities();

        let ball_entity = if let (Ok(ball), Ok(_)) = (balls.get(entity1), walls.get(entity2)) {
            ball
        } else if let (Ok(_), Ok(ball)) = (walls.get(entity1), balls.get(entity2)) {
            ball
        } else {
            continue;
        };

        commands.entity(ball_entity).despawn();
    }
}

pub fn reset_balls_system(
    mut commands: Commands,
    balls: Query<(), With<Ball>>,
    ball_container: Query<Entity, With<BallContainer>>,
    items: Query<Entity, With<AbilityPickup>>,
    ball_texture: Res<BallTextureHandleHolder>,
    mut level_data: ResMut<LevelData>,
    mut menu_state: ResMut<State<GameMenuState>>,
) {
    for _ in balls.iter() {
        return;
    }

    for item in items.iter() {
        commands.entity(item).despawn_recursive();
    }

    level_data.lives -= 1;
    level_data.score += -15;

    if level_data.lives == 0 {
        if let Err(error) = menu_state.set(GameMenuState::Lost) {
            eprintln!("Could not set state: {:?}", error);
        }
    }

    commands
        .entity(ball_container.single())
        .with_children(|parent| {
            parent
                .spawn()
                .insert_bundle(BallBundle::new_static(ball_texture.0.clone()))
                .insert(StickingBall::from(Vec2::new(0.0, 20.0)));
        });
}
