use crate::components::{AbilityBundle, AbilityPickup, Ball, BrickBundleGridBuilder};
use crate::{
    components::{Brick, BrickBundle},
    GameMenuState, Layer, LevelData, LevelSceneMarker,
};
use bevy::math::Vec3Swizzles;
use bevy::prelude::*;
use heron::prelude::*;
use rand::Rng;

impl Brick {
    /// Take damage. Not necessarily needed
    pub fn hit(&mut self) {}

    /// Check if brick is destroyed (in case it has multiple hits)
    pub fn is_destroyed(&self) -> bool {
        true
    }

    /// Returns the resource path for the brick variant
    pub fn get_asset_name(&self) -> &str {
        match self {
            Brick::Normal => "bricks/normal.png",
            _ => "",
        }
    }

    /// Meant for bricks, which should not be counted when checking if all bricks are gone
    pub fn exists(&self) -> bool {
        true
    }

    /// Gets the points that the brick is worth
    pub fn points(&self) -> i32 {
        match self {
            Brick::Normal => 40_i32,
            _ => 0,
        }
    }

    /// Determines if an item should be spawned when the brick is hit
    pub fn spawn_item(&self) -> Option<AbilityPickup> {
        if !self.is_destroyed() {
            return None;
        }

        match self {
            _ => AbilityPickup::from_number(rand::thread_rng().gen_range(0..10_u32)),
        }
    }
}

impl Default for Brick {
    fn default() -> Self {
        Brick::None
    }
}

impl BrickBundle {
    pub fn new(position: Vec2, brick: Brick, texture: Handle<Image>) -> Self {
        Self {
            body: RigidBody::Static,
            brick,
            layers: CollisionLayers::new(Layer::Bricks, Layer::Bricks),
            collision_shape: CollisionShape::Cuboid {
                half_extends: Vec3::new(30.0, 15.0, 0.0),
                border_radius: None,
            },
            material: PhysicMaterial {
                friction: 0.0,
                density: 1.0,
                restitution: 1.0,
            },
            sprite: SpriteBundle {
                transform: Transform::from_xyz(position.x, position.y, 0.0),
                global_transform: GlobalTransform::from_xyz(position.x, position.y, 0.0),
                sprite: Sprite {
                    custom_size: Some(Vec2::new(60.0, 30.0)),
                    ..Default::default()
                },
                texture,
                ..Default::default()
            },
        }
    }
}

impl BrickBundleGridBuilder {
    pub fn add(&mut self, texture: Handle<Image>, brick_type: Brick, gridx: f32, gridy: f32) {
        if let Brick::None = brick_type {
            return;
        }

        self.bricks.push(BrickBundle::new(
            Vec2::new((gridx - 9.0) * 62.0, gridy * 32.0),
            brick_type,
            texture,
        ));
    }

    pub fn all(self) -> Vec<BrickBundle> {
        self.bricks
    }
}

pub fn brick_hit_system(
    mut events: EventReader<CollisionEvent>,
    mut query: Query<(Entity, &mut Brick, &Transform)>,
    ball: Query<(Entity, &RigidBody), With<Ball>>,
    level_scene: Query<Entity, With<LevelSceneMarker>>,
    assets: Res<AssetServer>,
    mut commands: Commands,
    mut level_data: ResMut<LevelData>,
) {
    for event in events.iter() {
        if let CollisionEvent::Stopped(entity1, entity2) = event {
            let ball_body = if let Some((_, ball)) = ball
                .iter()
                .filter(|(entity, ..)| {
                    *entity == entity1.rigid_body_entity() || *entity == entity2.rigid_body_entity()
                })
                .next()
            {
                ball
            } else {
                continue;
            };

            let (brick_entity, mut brick, transform) = if let Some(entity_brick) = query
                .iter_mut()
                .filter(|(entity, ..)| {
                    *entity == entity1.rigid_body_entity() || *entity == entity2.rigid_body_entity()
                })
                .next()
            {
                entity_brick
            } else {
                continue;
            };

            if let RigidBody::Static = ball_body {
                continue;
            }

            brick.hit();

            if brick.is_destroyed() {
                commands.entity(brick_entity).despawn_recursive();
                level_data.score += brick.points();
            }

            if let (Some(item), Some(entity)) = (brick.spawn_item(), level_scene.iter().next()) {
                commands.entity(entity).with_children(|parent| {
                    parent.spawn_bundle(AbilityBundle::new(
                        assets.load(&item.sprite()),
                        item,
                        transform.translation.clone().xy(),
                    ));
                });
            }
        }
    }
}

pub fn finish_game_system(bricks: Query<&Brick>, mut menu_state: ResMut<State<GameMenuState>>) {
    for brick in bricks.iter() {
        if brick.exists() {
            return;
        }
    }

    if let Err(error) = menu_state.set(GameMenuState::Won) {
        eprintln!("Could not set state: {:?}", error);
    }
}
