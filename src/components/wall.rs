use crate::components::WallBundle;
use crate::Layer;
use bevy::prelude::*;
use heron::prelude::*;

impl WallBundle {
    pub fn new(position: Vec2, size: Vec2, texture: Handle<Image>) -> Self {
        Self {
            body: RigidBody::Static,
            collision_shape: CollisionShape::Cuboid {
                half_extends: Vec3::new(size.x / 2.0, size.y / 2.0, 0.0),
                border_radius: None,
            },
            layers: CollisionLayers::all::<Layer>(),
            material: PhysicMaterial {
                friction: 0.0,
                density: 1.0,
                restitution: 1.0,
            },
            sprite: SpriteBundle {
                sprite: Sprite {
                    custom_size: Some(size),
                    ..Default::default()
                },
                transform: Transform::from_xyz(position.x, position.y, 0.0),
                global_transform: GlobalTransform::from_xyz(position.x, position.y, 0.0),
                texture,
                ..Default::default()
            },
        }
    }
}
