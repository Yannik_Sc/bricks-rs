use crate::{
    components::{Pedal, PedalBundle, StickingBall},
    Layer,
};
use bevy::prelude::*;
use heron::prelude::*;
use std::ops::{DerefMut, Div, Sub};

impl Default for Pedal {
    fn default() -> Self {
        Self {
            width: 100.0,
            inverted: false,
            sticky: false,
        }
    }
}

impl PedalBundle {
    pub fn new(texture: Handle<Image>) -> Self {
        let pedal = Pedal::default();
        let pedal_size = Vec2::new(pedal.width, 20.0);

        Self {
            body: RigidBody::KinematicPositionBased,
            pedal,
            layers: CollisionLayers::all::<Layer>(),
            collision_shape: CollisionShape::Cuboid {
                half_extends: Vec3::new(pedal_size.x / 2.0, 0.0, 0.0),
                border_radius: Some(pedal_size.y / 2.0),
            },
            material: PhysicMaterial {
                restitution: PhysicMaterial::PERFECTLY_ELASTIC_RESTITUTION,
                density: 0.0000001,
                ..Default::default()
            },
            sprite: SpriteBundle {
                transform: Transform::from_xyz(0.0, -200.0, 0.0),
                global_transform: GlobalTransform::from_xyz(0.0, -200.0, 0.0),
                sprite: Sprite {
                    custom_size: Some(Vec2::new(1.0, 20.0)),
                    ..Default::default()
                },
                texture,
                ..Default::default()
            },
        }
    }
}

pub fn pedal_sync_system(mut query: Query<(&Pedal, &mut GlobalTransform, &mut CollisionShape)>) {
    for (pedal, mut transform, mut shape) in query.iter_mut() {
        transform.scale.x = pedal.width;

        if let CollisionShape::Cuboid { half_extends, .. } = shape.deref_mut() {
            half_extends.x = pedal.width / 2.0;
        }
    }
}

pub fn pedal_move_system(
    windows: Res<Windows>,
    mut pedal_query: Query<(&Pedal, &mut GlobalTransform)>,
) {
    let position = if let Some(window) = windows.get_primary() {
        if let Some(pos) = window.cursor_position() {
            pos
        } else {
            return;
        }
    } else {
        return;
    };

    let max_x = windows.get_primary().unwrap().width();

    if let Some((pedal, mut transform)) = pedal_query.iter_mut().next() {
        let mut newpos = if pedal.inverted {
            max_x.sub(&position.x)
        } else {
            position.x.clone()
        };

        newpos -= max_x.div(2.0);
        newpos = newpos.clamp(
            (pedal.width / 2.0) + 50.0 - (max_x.div(2.0)),
            (max_x / 2.0) - (pedal.width / 2.0) - 50.0,
        );

        let oldpos = transform.translation.x;

        let mut distance = oldpos - newpos;

        distance = distance.clamp(-pedal.width, pedal.width);

        transform.translation = Vec3::new(oldpos - distance, -200.0, 0.0);
    }
}

pub fn sync_pedal_position_to_ball_system(
    pedal_query: Query<(&Pedal, &GlobalTransform)>,
    ball_query: Query<(Entity, &GlobalTransform, &StickingBall)>,
    mut commands: Commands,
) {
    if let Some((_, pedal_transform)) = pedal_query.iter().next() {
        for (entity, ball_transform, sticky) in ball_query.iter() {
            let mut transform = *ball_transform;
            transform.translation = pedal_transform.translation + sticky.pedal_offset;

            commands
                .entity(entity)
                .remove::<Transform>()
                .insert(transform);
        }
    }
}

pub fn launch_ball_system(
    mut query: Query<Entity, (With<StickingBall>, With<RigidBody>)>,
    mut commands: Commands,
    input: Res<Input<KeyCode>>,
) {
    if !input.just_pressed(KeyCode::Space) {
        return;
    }

    for entity in query.iter_mut() {
        commands
            .entity(entity)
            .remove::<StickingBall>()
            .insert(RigidBody::Dynamic);
    }
}
