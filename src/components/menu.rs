use crate::components::{
    BuildLevelButtonMarker, GameExitButtonMarker, LevelExitButtonMarker, UiStyles,
};
use crate::{GameLevelState, GameMenuState};
use bevy::app::{AppExit, Events};
use bevy::prelude::*;

pub fn init_text_styles_system(mut commands: Commands, assets: Res<AssetServer>) {
    let normal_text = TextStyle {
        font: assets.load("fonts/acme7_wide.ttf"),
        font_size: 24.0,
        color: Color::WHITE,
    };
    let error_text = TextStyle {
        color: Color::RED,
        ..normal_text.clone()
    };
    let headline_text = TextStyle {
        font_size: 40.0,
        ..normal_text.clone()
    };
    let ui_root_container = Style {
        size: Size::new(Val::Percent(100.0), Val::Percent(100.0)),
        flex_direction: FlexDirection::ColumnReverse,
        align_content: AlignContent::Center,
        align_items: AlignItems::Center,
        padding: Rect {
            top: Val::Percent(8.0),
            ..Default::default()
        },
        ..Default::default()
    };
    let error_background = UiColor::from(Color::rgba(0.6, 0.0, 0.0, 0.2));
    let normal_background = UiColor::from(Color::rgba(0.0, 0.0, 0.0, 0.2));
    let overlay_menu_background = UiColor::from(Color::rgba(0.0, 0.0, 0.0, 0.4));
    let menu_background = UiColor::from(Color::rgb(0.6, 0.6, 0.6));

    commands.insert_resource(UiStyles {
        normal_text,
        error_text,
        headline_text,
        ui_root_container,
        error_background,
        normal_background,
        overlay_menu_background,
        menu_background,
    });
}

pub fn level_exit_button_click_system(
    interaction: Query<
        &Interaction,
        (
            Changed<Interaction>,
            With<Button>,
            With<LevelExitButtonMarker>,
        ),
    >,
    mut menu_state: ResMut<State<GameMenuState>>,
    mut build_level: ResMut<State<GameLevelState>>,
) {
    for interaction in interaction.iter() {
        if let Interaction::Clicked = interaction {
            if let Err(error) = menu_state.set(GameMenuState::Main) {
                eprintln!("Could not change end state: {:#?}", error);
            }

            if let Err(error) = build_level.set(GameLevelState::None) {
                eprintln!("Could not change level state: {:#?}", error);
            }
        }
    }
}

pub fn esc_menu(input: Res<Input<KeyCode>>, mut menu_state: ResMut<State<GameMenuState>>) {
    if !input.just_pressed(KeyCode::Escape) {
        return;
    }

    let result = match menu_state.current() {
        GameMenuState::Pause => menu_state.pop(),
        GameMenuState::None => menu_state.push(GameMenuState::Pause),
        _ => Ok(()),
    };

    if let Err(error) = result {
        eprintln!("Could not resume game: {:?}", error);
    }
}

pub fn main_menu_exit_button_click_system(
    interaction: Query<
        &Interaction,
        (
            Changed<Interaction>,
            With<Button>,
            With<GameExitButtonMarker>,
        ),
    >,
    mut exit_events: ResMut<Events<AppExit>>,
) {
    for interaction in interaction.iter() {
        if let Interaction::Clicked = interaction {
            exit_events.send(AppExit);
        }
    }
}

pub fn build_level_button_click_system(
    interaction: Query<
        (&Interaction, &BuildLevelButtonMarker),
        (Changed<Interaction>, With<Button>),
    >,
    mut menu_state: ResMut<State<GameMenuState>>,
    mut build_level: ResMut<State<GameLevelState>>,
) {
    if let Some((Interaction::Clicked, marker)) = interaction.iter().next() {
        if let Err(error) = build_level.push(marker.0.clone()) {
            eprintln!("Could not set build level: {:#?}", error);
        }

        if let Err(error) = menu_state.set(GameMenuState::None) {
            eprintln!("Could not set build level: {:#?}", error);
        }
    }
}
