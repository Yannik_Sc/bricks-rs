use crate::{GameLevelState, LevelAsset};
use bevy::prelude::*;
use heron::prelude::*;

use serde::Deserialize;

pub mod ball;
pub mod brick;
pub mod item;
pub mod level;
pub mod menu;
pub mod pedal;
pub mod wall;

pub mod systems {
    pub use crate::components::ball::{
        ball_hit_end_wall_system, ball_hit_pedal_system, ball_sync_system, reset_balls_system,
    };
    pub use crate::components::brick::{brick_hit_system, finish_game_system};
    pub use crate::components::item::item_collides_system;
    pub use crate::components::level::{
        load_level_system, pause_physics_system, resume_physics_system, show_scoreboard_system,
    };
    pub use crate::components::menu::{
        esc_menu, level_exit_button_click_system, main_menu_exit_button_click_system,
    };
    pub use crate::components::pedal::{
        launch_ball_system, pedal_move_system, pedal_sync_system,
        sync_pedal_position_to_ball_system,
    };
}

#[derive(Bundle, Clone)]
pub struct WallBundle {
    pub body: RigidBody,
    pub collision_shape: CollisionShape,
    pub layers: CollisionLayers,
    pub material: PhysicMaterial,
    #[bundle]
    pub sprite: SpriteBundle,
}

/// Marks walls that are killing the ball (most likely the bottom one)
#[derive(Clone, Component, Debug)]
pub struct EndWall;

#[derive(Clone, Component, Copy, Debug, Deserialize)]
pub enum Brick {
    None,
    Normal,
}

#[derive(Bundle, Clone)]
pub struct BrickBundle {
    pub body: RigidBody,
    pub brick: Brick,
    pub layers: CollisionLayers,
    pub collision_shape: CollisionShape,
    pub material: PhysicMaterial,
    #[bundle]
    pub sprite: SpriteBundle,
}

#[derive(Clone, Component, Debug)]
pub enum AbilityPickup {
    Invert,
    Faster,
    Slower,
    SplitBall,
}

#[derive(Bundle, Clone)]
pub struct AbilityBundle {
    pub body: RigidBody,
    pub collision_shape: CollisionShape,
    pub layers: CollisionLayers,
    pub material: PhysicMaterial,
    #[bundle]
    pub sprite: SpriteBundle,
    pub pickup: AbilityPickup,
    pub velocity: Velocity,
    pub rotation_constrains: RotationConstraints,
}

#[derive(Clone, Component, Default)]
pub struct BrickBundleGridBuilder {
    pub bricks: Vec<BrickBundle>,
}

#[derive(Clone, Component, Debug)]
pub struct Ball {
    pub size: f32,
}

#[derive(Clone, Component, Debug)]
pub struct BallTextureHandleHolder(pub Handle<Image>);

#[derive(Clone, Component, Debug)]
pub struct BallContainer;

#[derive(Bundle, Clone)]
pub struct BallBundle {
    pub ball: Ball,
    pub body: RigidBody,
    pub layers: CollisionLayers,
    pub collision_shape: CollisionShape,
    pub material: PhysicMaterial,
    pub velocity: Velocity,
    #[bundle]
    pub sprite: SpriteBundle,
}

#[derive(Clone, Component, Debug)]
pub struct StickingBall {
    pub pedal_offset: Vec3,
}

#[derive(Clone, Component, Debug)]
pub struct Pedal {
    pub width: f32,
    pub inverted: bool,
    pub sticky: bool,
}

#[derive(Bundle, Clone)]
pub struct PedalBundle {
    pub body: RigidBody,
    pub pedal: Pedal,
    pub layers: CollisionLayers,
    pub collision_shape: CollisionShape,
    pub material: PhysicMaterial,
    #[bundle]
    pub sprite: SpriteBundle,
}

#[derive(Clone, Component, Debug)]
pub struct LevelData {
    pub score: i32,
    pub lives: i32,
}

pub struct UiStyles {
    // Text Styles
    pub normal_text: TextStyle,
    pub error_text: TextStyle,
    pub headline_text: TextStyle,

    // Container Styles
    pub ui_root_container: Style,

    // Colors
    pub error_background: UiColor,
    pub normal_background: UiColor,
    pub overlay_menu_background: UiColor,
    pub menu_background: UiColor,
}

#[derive(Clone, Component, Debug)]
pub struct LevelDataText;

#[derive(Clone, Component, Debug)]
pub struct LevelSceneMarker;

#[derive(Clone, Component, Debug)]
pub struct LoadLevel {
    pub level_handle: Handle<LevelAsset>,
}

#[derive(Clone, Component, Debug)]
pub struct MenuMarker;

#[derive(Clone, Component, Debug)]
pub struct LevelExitButtonMarker;

#[derive(Clone, Component, Debug)]
pub struct GameExitButtonMarker;

#[derive(Clone, Component, Debug)]
pub struct BuildLevelButtonMarker(pub GameLevelState);
