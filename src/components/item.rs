use crate::components::{AbilityBundle, AbilityPickup, EndWall, Pedal};
use crate::Layer;
use bevy::prelude::*;
use heron::prelude::*;

impl AbilityPickup {
    pub fn from_number(num: u32) -> Option<Self> {
        match num {
            0 => Some(Self::Invert),
            1 => Some(Self::Faster),
            2 => Some(Self::Slower),
            3 => Some(Self::SplitBall),
            _ => None,
        }
    }

    pub fn sprite(&self) -> String {
        match self {
            AbilityPickup::Invert => String::from("items/red.png"),
            AbilityPickup::Faster => String::from("items/blue.png"),
            AbilityPickup::Slower => String::from("items/yellow.png"),
            AbilityPickup::SplitBall => String::from("items/green.png"),
        }
    }
}

impl AbilityBundle {
    pub fn new(texture: Handle<Image>, pickup: AbilityPickup, position: Vec2) -> Self {
        AbilityBundle {
            body: RigidBody::Dynamic,
            collision_shape: CollisionShape::Cuboid {
                half_extends: Vec3::new(15.0, 5.0, 0.0),
                border_radius: None,
            },
            layers: CollisionLayers::new(Layer::Items, Layer::Items),
            material: PhysicMaterial {
                restitution: PhysicMaterial::PERFECTLY_INELASTIC_RESTITUTION,
                ..Default::default()
            },
            sprite: SpriteBundle {
                sprite: Sprite {
                    custom_size: Some(Vec2::new(30.0, 10.0)),
                    ..Default::default()
                },
                transform: Transform::from_translation(position.extend(0.0)),
                global_transform: GlobalTransform::from(Transform::from_translation(
                    position.extend(0.0),
                )),
                texture,
                ..Default::default()
            },
            pickup,
            velocity: Velocity::from_linear(Vec3::new(0.0, -50.0, 0.0)),
            rotation_constrains: RotationConstraints::lock(),
        }
    }
}

pub fn item_collides_system(
    mut commands: Commands,
    mut events: EventReader<CollisionEvent>,
    items: Query<(Entity, &AbilityPickup)>,
    end_wall: Query<Entity, With<EndWall>>,
    mut pedal: Query<(Entity, &mut Pedal)>,
) {
    for collision in events.iter() {
        match collision {
            CollisionEvent::Started(entity1, entity2) => {
                let (item_entity, pickup) = if let Ok(item) = items
                    .get(entity1.rigid_body_entity())
                    .or_else(|_| items.get(entity2.rigid_body_entity()))
                {
                    item
                } else {
                    continue;
                };

                if let Ok(_) = end_wall.get(entity1.rigid_body_entity()) {
                    commands
                        .entity(entity2.rigid_body_entity())
                        .despawn_recursive();

                    continue;
                }
                if let Ok(_) = end_wall.get(entity2.rigid_body_entity()) {
                    commands
                        .entity(entity1.rigid_body_entity())
                        .despawn_recursive();

                    continue;
                }

                let mut pedal = if let Ok(pedal) = pedal.get_mut(entity1.rigid_body_entity()) {
                    pedal.1
                } else if let Ok(pedal) = pedal.get_mut(entity2.rigid_body_entity()) {
                    pedal.1
                } else {
                    continue;
                };

                commands.entity(item_entity).despawn_recursive();

                match pickup {
                    AbilityPickup::Invert => {
                        pedal.inverted = true;
                    }
                    AbilityPickup::Faster => {}
                    AbilityPickup::Slower => {}
                    AbilityPickup::SplitBall => {}
                }
            }
            _ => {}
        }
    }
}
