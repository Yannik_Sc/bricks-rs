use crate::components::{BrickBundleGridBuilder, LevelData, LevelDataText, LoadLevel};

use crate::{GameMenuState, LevelAsset, PhysicsTime};
use bevy::prelude::*;

impl Default for LevelData {
    fn default() -> Self {
        LevelData { score: 0, lives: 3 }
    }
}

pub fn show_scoreboard_system(
    level_data: Res<LevelData>,
    mut data: Query<&mut Text, With<LevelDataText>>,
) {
    for mut text in data.iter_mut() {
        if let Some(text) = text.sections.get_mut(0) {
            text.value = format!("Score: {} | Lives: {}", level_data.score, level_data.lives);
        };
    }
}

pub fn pause_physics_system(mut time: ResMut<PhysicsTime>) {
    time.pause();
}

pub fn resume_physics_system(mut time: ResMut<PhysicsTime>) {
    time.resume();
}

pub fn load_level_system(
    mut commands: Commands,
    mut asset_events: EventReader<AssetEvent<LevelAsset>>,
    query: Query<(Entity, &LoadLevel)>,
    levels: Res<Assets<LevelAsset>>,
    assets: Res<AssetServer>,
    mut game_state: ResMut<State<GameMenuState>>,
) {
    for asset_event in asset_events.iter() {
        match (asset_event, query.iter().next()) {
            (
                AssetEvent::Created { handle } | AssetEvent::Modified { handle },
                Some((entity, asset_handle)),
            ) => {
                if &asset_handle.level_handle != handle {
                    continue;
                }

                let level = if let Some(level) = levels.get(handle) {
                    level.clone()
                } else {
                    continue;
                };

                commands.entity(entity).with_children(|parent| {
                    let grid: BrickBundleGridBuilder = level.into_grid_builder(assets.clone());

                    for brick in grid.all() {
                        parent.spawn_bundle(brick);
                    }
                });

                if let GameMenuState::LevelLoading = game_state.current() {
                    if let Err(error) = game_state.pop() {
                        eprintln!("Could not pop loading state: {:#?}", error);
                    }
                }
            }
            _ => {}
        }
    }
}
