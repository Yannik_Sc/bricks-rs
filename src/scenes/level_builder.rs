use crate::components::{
    BallBundle, BallContainer, BallTextureHandleHolder, LevelDataText, LevelSceneMarker, LoadLevel,
    StickingBall,
};
use crate::{components::PedalBundle, GameLevelState, GameMenuState, LevelData, ToLevelInfo};
use bevy::asset::AssetPath;
use bevy::prelude::*;

fn build_game_elements<'a, S: Into<AssetPath<'a>>>(
    mut commands: Commands,
    assets: Res<AssetServer>,
    window: Res<WindowDescriptor>,
    level_path: S,
    ball_texture: Res<BallTextureHandleHolder>,
) {
    commands.insert_resource(LevelData::default());

    commands
        .spawn()
        .insert(LevelSceneMarker)
        .insert(LoadLevel {
            level_handle: assets.load(level_path),
        })
        .with_children(|parent| {
            parent
                .spawn()
                .insert_bundle(Text2dBundle {
                    text: Text::with_section(
                        "Score",
                        TextStyle {
                            font_size: 24.0,
                            color: Color::WHITE,
                            font: assets.load("fonts/acme7_wide.ttf"),
                            ..Default::default()
                        },
                        TextAlignment::default(),
                    ),
                    global_transform: GlobalTransform::from_xyz(
                        0.0,
                        (window.height / 2.0) - 18.0,
                        10.0,
                    ),
                    ..Default::default()
                })
                .insert(LevelDataText);

            parent
                .spawn()
                .insert(BallContainer)
                .with_children(|parent| {
                    parent
                        .spawn()
                        .insert_bundle(BallBundle::new_static(ball_texture.0.clone()))
                        .insert(StickingBall::from(Vec2::new(0.0, 20.0)));
                });

            parent
                .spawn()
                .insert_bundle(PedalBundle::new(assets.load("pedal.png")));
        });
}

pub fn build_level_from_state_system(
    commands: Commands,
    assets: Res<AssetServer>,
    window: Res<WindowDescriptor>,
    ball_texture: Res<BallTextureHandleHolder>,
    level_build_state: Res<State<GameLevelState>>,
    mut game_state: ResMut<State<GameMenuState>>,
) {
    build_game_elements(
        commands,
        assets,
        window,
        level_build_state.current().get_level_path(),
        ball_texture,
    );

    if let Err(error) = game_state.push(GameMenuState::LevelLoading) {
        eprintln!("Could start level loading: {:#?}", error);
    }
}
