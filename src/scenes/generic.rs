use crate::components::{
    BallTextureHandleHolder, BuildLevelButtonMarker, EndWall, GameExitButtonMarker,
    LevelExitButtonMarker, MenuMarker, UiStyles, WallBundle,
};
use crate::states::ToLevelInfo;
use crate::{Commands, GameLevelState, LevelData, OrthographicCameraBundle, Transform, Vec2};
use bevy::ecs::component::Component;
use bevy::prelude::*;
use bevy::ui::entity::NodeBundle;

trait TextAlignCenter {
    fn center() -> TextAlignment;
}

trait RectConstructions<T: Reflect + PartialEq> {
    fn top(top: T) -> Rect<T>;
    fn bottom(bottom: T) -> Rect<T>;
    fn left(left: T) -> Rect<T>;
    fn right(right: T) -> Rect<T>;
    fn vertical(vertical: T) -> Rect<T>;
    fn horizontal(horizontal: T) -> Rect<T>;
}

impl TextAlignCenter for TextAlignment {
    fn center() -> TextAlignment {
        TextAlignment {
            vertical: VerticalAlign::Center,
            horizontal: HorizontalAlign::Center,
        }
    }
}

impl<T: Reflect + PartialEq + Clone + Default> RectConstructions<T> for Rect<T> {
    fn top(top: T) -> Rect<T> {
        Rect {
            top,
            ..Default::default()
        }
    }

    fn bottom(bottom: T) -> Rect<T> {
        Rect {
            bottom,
            ..Default::default()
        }
    }

    fn left(left: T) -> Rect<T> {
        Rect {
            left,
            ..Default::default()
        }
    }

    fn right(right: T) -> Rect<T> {
        Rect {
            right,
            ..Default::default()
        }
    }

    fn vertical(vertical: T) -> Rect<T> {
        Rect {
            left: vertical.clone(),
            right: vertical,
            ..Default::default()
        }
    }

    fn horizontal(horizontal: T) -> Rect<T> {
        Rect {
            top: horizontal.clone(),
            bottom: horizontal,
            ..Default::default()
        }
    }
}

pub fn preload_resources_system(mut commands: Commands, assets: Res<AssetServer>) {
    commands.insert_resource(BallTextureHandleHolder(assets.load("ball.png")));
}

pub fn build_level_system(
    mut commands: Commands,
    window: Res<WindowDescriptor>,
    assets: Res<AssetServer>,
) {
    commands
        .spawn()
        .insert_bundle(OrthographicCameraBundle::new_2d())
        .insert(Transform::from_xyz(0.0, 0.0, 1000.0));

    commands.spawn().with_children(|parent| {
        parent.spawn().insert_bundle(WallBundle::new(
            Vec2::new(0.0, window.height / 2.0),
            Vec2::new(window.width, 100.0),
            assets.load("wall.png"),
        ));

        parent
            .spawn()
            .insert_bundle(WallBundle::new(
                Vec2::new(0.0, window.height / -2.0),
                Vec2::new(window.width, 100.0),
                assets.load("wall.png"),
            ))
            .insert(EndWall);

        parent.spawn().insert_bundle(WallBundle::new(
            Vec2::new(window.width / -2.0, 0.0),
            Vec2::new(100.0, window.height),
            assets.load("wall.png"),
        ));

        parent.spawn().insert_bundle(WallBundle::new(
            Vec2::new(window.width / 2.0, 0.0),
            Vec2::new(100.0, window.height),
            assets.load("wall.png"),
        ));
    });
}

pub fn show_pause_menu_system(mut commands: Commands, ui_styles: Res<UiStyles>) {
    build_base_menu(
        &mut commands,
        &ui_styles,
        || MenuMarker,
        |parent| {
            parent.spawn_bundle(TextBundle {
                text: Text::with_section(
                    "Paused",
                    ui_styles.headline_text.clone(),
                    TextAlignment::center(),
                ),
                style: Style {
                    margin: Rect::bottom(Val::Px(40.0)),
                    ..Default::default()
                },
                ..Default::default()
            });

            parent
                .spawn_bundle(ButtonBundle {
                    color: ui_styles.error_background.clone(),
                    style: Style {
                        padding: Rect::all(Val::Px(12.0)),
                        ..Default::default()
                    },
                    ..Default::default()
                })
                .insert(LevelExitButtonMarker)
                .with_children(|parent| {
                    parent.spawn_bundle(TextBundle {
                        text: Text::with_section(
                            "Exit to Main Menu",
                            ui_styles.error_text.clone(),
                            TextAlignment::center(),
                        ),
                        ..Default::default()
                    });
                });
        },
    );
}

pub fn show_main_menu_system(mut commands: Commands, ui_styles: Res<UiStyles>) {
    commands
        .spawn_bundle(UiCameraBundle::default())
        .insert(MenuMarker);

    commands
        .spawn_bundle(NodeBundle {
            color: ui_styles.menu_background.clone(),
            style: ui_styles.ui_root_container.clone(),
            ..Default::default()
        })
        .insert(MenuMarker)
        .with_children(|parent| {
            parent.spawn_bundle(TextBundle {
                text: Text::with_section(
                    "Bricks",
                    ui_styles.headline_text.clone(),
                    TextAlignment::center(),
                ),
                style: Style {
                    margin: Rect::bottom(Val::Px(40.0)),
                    ..Default::default()
                },
                ..Default::default()
            });

            build_normal_button(
                parent,
                &ui_styles,
                "Start Demo Level",
                BuildLevelButtonMarker(GameLevelState::Demo),
            );

            build_normal_button(
                parent,
                &ui_styles,
                "Start First Level",
                BuildLevelButtonMarker(GameLevelState::Level1),
            );

            build_error_button(parent, &ui_styles, "Quit to desktop", GameExitButtonMarker);
        });
}

pub fn show_lose_screen_system(mut commands: Commands, ui_styles: Res<UiStyles>) {
    build_base_menu(
        &mut commands,
        &ui_styles,
        || MenuMarker,
        |parent| {
            parent.spawn_bundle(TextBundle {
                text: Text::with_section(
                    "You Lose :(",
                    ui_styles.headline_text.clone(),
                    TextAlignment::center(),
                ),
                style: Style {
                    margin: Rect::bottom(Val::Px(40.0)),
                    ..Default::default()
                },
                ..Default::default()
            });

            build_error_button(parent, &ui_styles, "To Main Menu", LevelExitButtonMarker);
        },
    );
}

pub fn show_won_screen_system(
    mut commands: Commands,
    ui_styles: Res<UiStyles>,
    data: Res<LevelData>,
    game: Res<State<GameLevelState>>,
) {
    build_base_menu(
        &mut commands,
        &ui_styles,
        || MenuMarker,
        |parent| {
            parent.spawn_bundle(TextBundle {
                text: Text::with_section(
                    "You won!",
                    ui_styles.headline_text.clone(),
                    TextAlignment::center(),
                ),
                style: Style {
                    margin: Rect::bottom(Val::Px(12.0)),
                    ..Default::default()
                },
                ..Default::default()
            });

            parent.spawn_bundle(TextBundle {
                text: Text::with_section(
                    format!("With a score of {} points!", data.score),
                    ui_styles.normal_text.clone(),
                    TextAlignment::center(),
                ),
                style: Style {
                    margin: Rect::bottom(Val::Px(40.0)),
                    ..Default::default()
                },
                ..Default::default()
            });

            match game.current() {
                GameLevelState::None => {}
                state => build_normal_button(
                    parent,
                    &ui_styles,
                    "Next Level",
                    BuildLevelButtonMarker(state.next_level()),
                ),
            }

            build_normal_button(parent, &ui_styles, "To Main Menu", LevelExitButtonMarker);
        },
    );
}

pub fn despawn_marked_system<T: 'static + Component>(
    mut commands: Commands,
    marked: Query<Entity, With<T>>,
) {
    for entity in marked.iter() {
        commands.entity(entity).despawn_recursive();
    }
}

fn build_error_button<C: Component, T: Into<String>>(
    parent: &mut ChildBuilder,
    ui_styles: &UiStyles,
    text: T,
    marker: C,
) {
    build_button(
        parent,
        ui_styles.error_background.clone(),
        ui_styles.error_text.clone(),
        text,
        marker,
    );
}

fn build_normal_button<C: Component, T: Into<String>>(
    parent: &mut ChildBuilder,
    ui_styles: &UiStyles,
    text: T,
    marker: C,
) {
    build_button(
        parent,
        ui_styles.normal_background.clone(),
        ui_styles.normal_text.clone(),
        text,
        marker,
    );
}

fn build_button<C: Component, T: Into<String>>(
    parent: &mut ChildBuilder,
    background: UiColor,
    text_style: TextStyle,
    text: T,
    marker: C,
) {
    parent
        .spawn_bundle(ButtonBundle {
            color: background,
            style: Style {
                padding: Rect::all(Val::Px(12.0)),
                margin: Rect::bottom(Val::Px(12.0)),
                ..Default::default()
            },
            ..Default::default()
        })
        .insert(marker)
        .with_children(|parent| {
            parent.spawn_bundle(TextBundle {
                text: Text::with_section(text, text_style, TextAlignment::center()),
                ..Default::default()
            });
        });
}

fn build_base_menu<M: Component>(
    commands: &mut Commands,
    ui_styles: &UiStyles,
    marker: impl Fn() -> M,
    factory: impl FnOnce(&mut ChildBuilder),
) -> Entity {
    commands
        .spawn_bundle(UiCameraBundle::default())
        .insert((marker)());
    commands
        .spawn()
        .insert_bundle(NodeBundle {
            color: ui_styles.overlay_menu_background.clone(),
            style: ui_styles.ui_root_container.clone(),
            ..Default::default()
        })
        .insert((marker)())
        .with_children(factory)
        .id()
}
