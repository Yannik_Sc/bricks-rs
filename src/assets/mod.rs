use crate::components::Brick;
use bevy::reflect::TypeUuid;
use serde::Deserialize;

mod level_loader;

#[derive(Default)]
pub struct LevelAssetLoader;

#[derive(Clone, Debug, Deserialize, TypeUuid)]
#[uuid = "79f64149-0b91-42cf-8098-a2cebf7dfe94"]
pub struct LevelAsset {
    bricks: Vec<Vec<Brick>>,
}
