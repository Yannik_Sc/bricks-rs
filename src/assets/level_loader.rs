use crate::assets::{LevelAsset, LevelAssetLoader};
use crate::components::BrickBundleGridBuilder;
use bevy::asset::{AssetLoader, BoxedFuture, LoadContext, LoadedAsset};
use bevy::prelude::AssetServer;

impl AssetLoader for LevelAssetLoader {
    fn load<'a>(
        &'a self,
        bytes: &'a [u8],
        load_context: &'a mut LoadContext,
    ) -> BoxedFuture<'a, anyhow::Result<(), anyhow::Error>> {
        Box::pin(async move {
            let res: LevelAsset = ron::de::from_bytes(bytes)?;
            load_context.set_default_asset(LoadedAsset::new(res));

            Ok(())
        })
    }

    fn extensions(&self) -> &[&str] {
        &["level"]
    }
}

impl LevelAsset {
    pub fn into_grid_builder(self, assets: AssetServer) -> BrickBundleGridBuilder {
        let mut grid_builder = BrickBundleGridBuilder::default();

        for row in 0..self.bricks.len() {
            let rowv = &self.bricks[row];

            for col in 0..rowv.len() {
                let brick = rowv[col];

                grid_builder.add(
                    assets.load(brick.get_asset_name()),
                    brick,
                    col as f32,
                    row as f32,
                );
            }
        }

        grid_builder
    }
}
