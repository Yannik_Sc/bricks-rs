use crate::{GameLevelState, ToLevelInfo};

impl ToLevelInfo for GameLevelState {
    fn get_level_path(&self) -> &str {
        match self {
            GameLevelState::Demo => "levels/demo.level",
            GameLevelState::Level1 => "levels/first.level",
            _ => "",
        }
    }

    fn next_level(&self) -> Self {
        match self {
            GameLevelState::Demo => GameLevelState::Level1,
            _ => GameLevelState::None,
        }
    }
}
