mod level_state;

use std::fmt::Debug;
use std::hash::Hash;

pub trait ToLevelInfo {
    fn get_level_path(&self) -> &str;

    fn next_level(&self) -> Self;
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum GameMenuState {
    Lost,
    Won,
    Main,
    Pause,
    LevelLoading,
    None,
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum GameLevelState {
    None,
    Demo,
    Level1,
    Level2,
}
