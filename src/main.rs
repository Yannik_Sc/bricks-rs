use bevy::prelude::*;
use heron::prelude::*;

use crate::assets::{LevelAsset, LevelAssetLoader};
use crate::components::menu::{build_level_button_click_system, init_text_styles_system};
use crate::components::MenuMarker;
use crate::components::{LevelData, LevelSceneMarker};
use crate::scenes::generic::{
    despawn_marked_system, preload_resources_system, show_lose_screen_system,
    show_main_menu_system, show_pause_menu_system, show_won_screen_system,
};
use crate::scenes::{generic::build_level_system, level_builder::build_level_from_state_system};
use components::systems::*;
pub use states::*;

mod assets;
mod components;
mod scenes;
mod states;

pub struct BricksPlugin;

#[derive(PhysicsLayer)]
pub enum Layer {
    Bricks,
    Items,
}

impl BricksPlugin {
    fn initialize_resources(&self, builder: &mut App) {
        builder
            .add_state(GameMenuState::Main)
            .add_state(GameLevelState::None)
            .insert_resource(ClearColor(Color::rgb(0.9, 0.9, 0.9)))
            .add_asset::<LevelAsset>()
            .init_asset_loader::<LevelAssetLoader>()
            .init_resource::<LevelData>();
    }

    fn add_menu_systems(&self, builder: &mut App) {
        builder
            .add_system_set(
                SystemSet::on_update(GameMenuState::None)
                    .with_system(ball_sync_system)
                    .with_system(brick_hit_system)
                    .with_system(pedal_move_system)
                    .with_system(pedal_sync_system)
                    .with_system(ball_hit_pedal_system)
                    .with_system(launch_ball_system)
                    .with_system(ball_hit_end_wall_system)
                    .with_system(reset_balls_system)
                    .with_system(show_scoreboard_system)
                    .with_system(finish_game_system)
                    .with_system(item_collides_system)
                    .with_system(sync_pedal_position_to_ball_system),
            )
            .add_system_set(
                SystemSet::on_update(GameMenuState::Pause)
                    .with_system(level_exit_button_click_system),
            )
            .add_system_set(
                SystemSet::on_enter(GameMenuState::Pause)
                    .with_system(pause_physics_system)
                    .with_system(despawn_marked_system::<MenuMarker>)
                    .with_system(show_pause_menu_system),
            )
            .add_system_set(
                SystemSet::on_exit(GameMenuState::Pause)
                    .with_system(despawn_marked_system::<MenuMarker>)
                    .with_system(resume_physics_system),
            )
            .add_system_set(
                SystemSet::on_enter(GameMenuState::None)
                    .with_system(despawn_marked_system::<MenuMarker>),
            )
            .add_system_set(
                SystemSet::on_enter(GameMenuState::Main)
                    .with_system(despawn_marked_system::<MenuMarker>)
                    .with_system(show_main_menu_system),
            )
            .add_system_set(
                SystemSet::on_update(GameMenuState::LevelLoading).with_system(load_level_system),
            )
            .add_system_set(
                SystemSet::on_update(GameMenuState::Main)
                    .with_system(build_level_button_click_system)
                    .with_system(main_menu_exit_button_click_system),
            )
            .add_system_set(
                SystemSet::on_enter(GameMenuState::Lost).with_system(show_lose_screen_system),
            )
            .add_system_set(
                SystemSet::on_update(GameMenuState::Lost)
                    .with_system(level_exit_button_click_system),
            )
            .add_system_set(
                SystemSet::on_enter(GameMenuState::Won).with_system(show_won_screen_system),
            )
            .add_system_set(
                SystemSet::on_update(GameMenuState::Won)
                    .with_system(build_level_button_click_system)
                    .with_system(level_exit_button_click_system),
            )
            .add_system(esc_menu);
    }

    fn add_level_systems(&self, builder: &mut App) {
        builder
            .add_system_set(
                SystemSet::on_enter(GameLevelState::None)
                    .with_system(despawn_marked_system::<LevelSceneMarker>),
            )
            .add_system_set(
                SystemSet::on_enter(GameLevelState::Demo)
                    .with_system(despawn_marked_system::<LevelSceneMarker>)
                    .with_system(build_level_from_state_system),
            )
            .add_system_set(
                SystemSet::on_enter(GameLevelState::Level1)
                    .with_system(despawn_marked_system::<LevelSceneMarker>)
                    .with_system(build_level_from_state_system),
            );
    }
}

impl Plugin for BricksPlugin {
    fn build(&self, builder: &mut App) {
        self.initialize_resources(builder);
        self.add_menu_systems(builder);
        self.add_level_systems(builder);

        builder
            .add_startup_system(init_text_styles_system)
            .add_startup_system(preload_resources_system)
            .add_startup_system(build_level_system);
    }

    fn name(&self) -> &str {
        "Bricks"
    }
}

fn main() {
    App::new()
        .insert_resource(WindowDescriptor {
            title: format!("{} v{}", env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION")),
            resizable: false,
            width: 1280.,
            height: 720.,
            ..Default::default()
        })
        .add_plugins(DefaultPlugins)
        .add_plugin(PhysicsPlugin::default())
        .add_plugin(BricksPlugin)
        .run();
}
