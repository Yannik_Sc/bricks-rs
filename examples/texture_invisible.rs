//!
//! # The problem
//!
//! This example shows a texture loading bug in which a texture, which was before used by another
//! entity, is now used in a new entity, which causes it to be invisible
//!
//! # My guess
//!
//! My guess is that the asset server is detecting that the texture is not needed anymore, so it
//! starts to unload it. But before it is unloaded, the texture is already used in another spawn,
//! where it detects, that the texture is still loaded, so it does nothing.
//!

use bevy::prelude::*;

#[derive(Component)]
struct TextureAdder {
    pub texture: String,
}

fn add_texture(
    mut commands: Commands,
    textures: Query<(Entity, &GlobalTransform, &TextureAdder)>,
    assets: Res<AssetServer>,
) {
    for (entity, transform, texture) in textures.iter() {
        commands
            .entity(entity)
            .insert_bundle(SpriteBundle {
                sprite: Sprite {
                    custom_size: Some(Vec2::new(20.0, 20.0)),
                    color: Color::RED,
                    ..Default::default()
                },
                texture: assets.load(&texture.texture),
                global_transform: transform.clone(),
                ..Default::default()
            })
            .remove::<TextureAdder>();
    }
}

fn startup(mut commands: Commands) {
    commands.spawn_bundle(OrthographicCameraBundle::new_2d());
    commands.spawn();
}

fn key_events(
    mut commands: Commands,
    parent: Query<Entity, (Without<Parent>, Without<Camera>)>,
    input: Res<Input<KeyCode>>,
    windows: Res<Windows>,
) {
    if input.just_pressed(KeyCode::D) || input.just_pressed(KeyCode::R) {
        commands.entity(parent.single()).despawn_descendants();
    }

    if !input.just_pressed(KeyCode::S) && !input.just_pressed(KeyCode::R) {
        return;
    }

    let window = windows.get_primary().unwrap();
    let window_center = Vec2::new(window.width(), window.height()) / 2.0;

    if let Some(cursor) = window.cursor_position() {
        let position: Vec2 = cursor - window_center;
        commands.entity(parent.single()).with_children(|parent| {
            parent
                .spawn()
                .insert(GlobalTransform::from(Transform::from_translation(
                    position.extend(0.0),
                )))
                .insert(TextureAdder {
                    texture: String::from("ball.png"),
                })
                .insert(GlobalTransform::from(Transform::from_translation(
                    position.extend(0.0),
                )));
        });
    }
}

pub fn main() {
    App::new()
        .insert_resource(WindowDescriptor {
            title: format!(
                "{} v{} - Texture not loading example",
                env!("CARGO_PKG_NAME"),
                env!("CARGO_PKG_VERSION")
            ),
            resizable: false,
            width: 1270.0,
            height: 720.0,
            ..Default::default()
        })
        .add_system(key_events)
        .add_system(add_texture)
        .add_startup_system(startup)
        .add_plugins(DefaultPlugins)
        .run()
}
