use bevy::prelude::*;
use heron::prelude::*;

pub const SCREEN_WIDTH: f32 = 1280.0;
pub const SCREEN_HEIGHT: f32 = 720.0;

#[derive(Component)]
struct KinematicMoving;

fn setup_scene_system(mut commands: Commands) {
    commands
        .spawn()
        .insert_bundle(OrthographicCameraBundle::new_2d())
        .insert(Transform::from_xyz(0.0, 0.0, 1000.0));

    commands
        .spawn()
        .insert_bundle(SpriteBundle {
            sprite: Sprite {
                custom_size: Some(Vec2::new(20.0, 20.0)),
                color: Color::RED,
                ..Default::default()
            },
            transform: Transform::from_xyz(200.0, 0.0, 0.0),

            ..Default::default()
        })
        .insert(RigidBody::Dynamic)
        .insert(CollisionShape::Cuboid {
            half_extends: Vec3::new(10.0, 10.0, 1000.0),
            border_radius: None,
        });

    commands
        .spawn()
        .insert_bundle(SpriteBundle {
            sprite: Sprite {
                custom_size: Some(Vec2::new(20.0, 20.0)),
                color: Color::RED,
                ..Default::default()
            },
            transform: Transform::from_xyz(0.0, 10.0, 0.0),

            ..Default::default()
        })
        .insert(RigidBody::KinematicPositionBased)
        .insert(KinematicMoving)
        .insert(CollisionShape::Cuboid {
            half_extends: Vec3::new(10.0, 10.0, 1000.0),
            border_radius: None,
        });

    commands
        .spawn()
        .insert_bundle(SpriteBundle {
            sprite: Sprite {
                custom_size: Some(Vec2::new(20.0, 20.0)),
                color: Color::RED,
                ..Default::default()
            },
            transform: Transform::from_xyz(200.0, 200.0, 0.0),

            ..Default::default()
        })
        .insert(PhysicMaterial {
            density: 1000.0,
            friction: 1.0,
            restitution: 1.0,
        })
        .insert(RigidBody::Dynamic)
        .insert(CollisionShape::Cuboid {
            half_extends: Vec3::new(10.0, 10.0, 1000.0),
            border_radius: None,
        });

    commands
        .spawn()
        .insert_bundle(SpriteBundle {
            sprite: Sprite {
                custom_size: Some(Vec2::new(20.0, 20.0)),
                color: Color::RED,
                ..Default::default()
            },
            transform: Transform::from_xyz(0.0, 210.0, 0.0),

            ..Default::default()
        })
        .insert(RigidBody::KinematicPositionBased)
        .insert(KinematicMoving)
        .insert(CollisionShape::Cuboid {
            half_extends: Vec3::new(10.0, 10.0, 1000.0),
            border_radius: None,
        });
}

fn move_kinematic_system(mut blocks: Query<&mut GlobalTransform, With<KinematicMoving>>) {
    for mut block in blocks.iter_mut() {
        block.translation.x += 1.0;
    }
}

fn key_events_system(input: Res<Input<KeyCode>>, mut time: ResMut<PhysicsTime>) {
    if input.just_pressed(KeyCode::R) {
        time.resume();
    }

    if input.just_pressed(KeyCode::P) {
        time.pause();
    }
}

pub fn main() {
    App::new()
        .insert_resource(WindowDescriptor {
            title: format!(
                "{} v{} - Physics Bug Example",
                env!("CARGO_PKG_NAME"),
                env!("CARGO_PKG_VERSION")
            ),
            resizable: false,
            width: SCREEN_WIDTH,
            height: SCREEN_HEIGHT,
            ..Default::default()
        })
        .add_system(key_events_system)
        .add_system(move_kinematic_system)
        .add_startup_system(setup_scene_system)
        .add_plugins(DefaultPlugins)
        .add_plugin(PhysicsPlugin::default())
        .run()
}
